import React from 'react'
import Moment from 'react-moment'
import {Vids,TwoColumn,VideoCont} from '../Common/Styles'
import {FaRegClock,FaCloudUploadAlt,FaTag,FaEye} from 'react-icons/fa'

export default (props)=>(
    <Vids type={props.type}>
       
    {console.log(props.allItems)}
             {props.allItems && props.allItems.map((i,key)=> {
             return   (
                 <li key={key}>
                    <span className='textTime'>
                        <FaRegClock /> <Moment fromNow>{i.snippet.publishedAt}</Moment>
                    </span>
                    <VideoCont>
                    <iframe className='frame' src={'https://www.youtube.com/embed/'+i.id}
                                frameBorder='0'
                                allow='autoplay; encrypted-media'
                                allowFullScreen
                                title='video'
                        />
                    </VideoCont>
                    <div className="vidTitle">{i.snippet.title}</div>
                    <TwoColumn>
                        <li ><FaEye /></li>
                        <li >{i.statistics.viewCount} Views</li>
                    </TwoColumn>
                    <div className='Desc'>{i.snippet.description ? i.snippet.description.substr(0, 200): "No Available Description"} ...</div>
                    <TwoColumn>
                        <li>
                        <FaCloudUploadAlt />
                        </li>
                        <li>
                            <Moment fromNow>{i.snippet.publishedAt}</Moment>
                        </li>
                    </TwoColumn> 
                    <TwoColumn>
                        <li><FaTag /> JV#123456</li>
                        <li> </li>
                    </TwoColumn>
                    <div className='licenseBtn'>License This Video</div>
                 </li>
             )
         })
     
     } 
      </Vids>
)