import React from 'react'
import {withContext} from '../Common/withContext'
import InfiniteScroll from 'react-infinite-scroll-component'
import Card from '../Card'
 
class Main extends React.PureComponent {

    constructor(props) {
        super(props);
        this.state = { 
            data:{},
            allItems:[],
            nextPageToken:undefined,
            loader:"Loading",
            filter:props.state.filter
        };
      }
      async componentDidMount() {
          console.log(this.props.state.type)
          this.fetchData()
        
          

    }


    componentWillUnmount = ()=>{
        const controller = new AbortController();
        controller.abort()
        this.setState = undefined;

    }


    filterList =()=>{

       // const sdate = "2019-05-06T00:00:00.000Z";
     //  console.log("todayweek:",this.props.state.filter)

       
        let sdate;
        let x = 0;
        if(this.props.state.filter==="week"){
            x = 7   
        }

        if(this.props.state.filter==="day"){
           x = 1
        }

        console.log(this.props.state.filter)
        const ed = new Date();
        let today = new Date();

        let sd = new Date();
        sd.setDate(today.getDate() - x)



        const result = this.state.allItems && this.state.allItems.filter(d => {var time = new Date(d.snippet.publishedAt).getTime();
                             return (sd < time && time < ed);
                            });
                            
        console.log("ed",ed)
        console.log("sd:",sd)
        console.log('res',result)

        //return result
      //  this.setState({allItems:result})
    }

    fetchData = async ()=>{
        const PT = this.state.nextPageToken;
        const k='AIzaSyCOlZK128Xidm-FzFAKa99mi23Iy1wCBv8';
        let response;
        let json;

       // if(this.props.state.filter === "all"){
            if(PT !== undefined){
                response = await fetch(`https://www.googleapis.com/youtube/v3/videos?part=snippet%2CcontentDetails%2Cstatistics%2Cplayer&chart=mostPopular&regionCode=US&maxResults=10&pageToken=${PT}&key=${k}`)
                json = await response.json();
                const {items,nextPageToken} = json;
                this.setState({ allItems: [...this.state.allItems,...items], nextPageToken});
            }
            else{
                response = await fetch(`https://www.googleapis.com/youtube/v3/videos?part=snippet%2CcontentDetails%2Cstatistics%2Cplayer&chart=mostPopular&regionCode=US&maxResults=10&key=${k}`)
                json = await response.json();
                const {items,nextPageToken} = json;
                this.setState({allItems:items,nextPageToken})
            }
       /*  }else{
            this.filterList(this.props.state.filter)
        } */
    }

   
      render() {
        this.filterList()

         return (
 
           <InfiniteScroll
                    dataLength={this.state.allItems && this.state.allItems.length} 
                    //This is important field to render the next data
                    next={this.fetchData}
                    hasMore={true}
                    loader={<h4>{this.state.loader}</h4>}
                    endMessage={""}
                    // below props only if you need pull down functionality
                     /* refreshFunction={this.fetchData(this.state.nextPageToken)}
                    pullDownToRefreshContent={
                        <h3 style={{textAlign: 'center'}}>&#8595; Pull down to refresh</h3>
                    }
                    releaseToRefreshContent={
                        <h3 style={{textAlign: 'center'}}>&#8593; Release to refresh</h3>
                    } */ >

                    <Card type={this.props.state.type} allItems={this.state.allItems} />
                    
            </InfiniteScroll>

        );
      }
  
}

export default withContext(Main)