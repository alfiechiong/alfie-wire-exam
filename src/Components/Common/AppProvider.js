import React,{Component,createContext} from 'react';

export const {
    Provider,
    Consumer
  } = createContext();


  class AppProvider extends Component {
    state = {
      type : "List",
      filter:"all"
    }

  render() {
      return (
        <Provider value={{
            type: this.state.type,
            filter:this.state.filter,
            setType:(type)=>{
                this.setState({type})
            },
            setFilter:(filter)=>{
              this.setState({filter})
          }
        }}
        >
        {this.props.children}
        </Provider>
      )
  }
}
  export default AppProvider