import React from 'react'
import {HeaderStyled} from './Styles'

export default ()=>(
    <HeaderStyled>
    <img src="https://www.jukinmedia.com/assets/images/thewire/thewire-logo.png" alt="The Wire"></img>
    <div>
        YOUR <span>REAL-TIME</span> SOURCE FOR THE WORLD'S BEST USER-GENERATED VIDEOS...<span>BEFORE</span> THEY GO VIRAL.
    </div>
    </HeaderStyled>
)