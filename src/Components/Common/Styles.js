import styled from 'styled-components' 

export const NavStyled = styled.div`
    text-align: center;
    display:flex;

    
    
    padding:10px;
 
    justify-content: space-between;
    align-items:center;
    border-bottom:solid thin #ccc;

   

`

export const HeaderStyled = styled.div`
    text-align: center;
    margin:0px auto;
    overflow:auto;
    clear:both;
    padding:10px;
    background:#f04f38;
    height:100px;

    div{
        color:white;
        font-size: 1.3ch;
        text-transform: uppercase;
        margin-top: 10px;
        font-weight: 400;
        
        &>span{
            font-weight: 700;
            border-bottom: 2px solid #fff;
        }
    }
`

export const LStyled = styled.div`
    flex:1;
    justify-content:center;
    display:flex;

    @media only screen and (max-width: 600px){
        flex-direction:column;
    }
        
    &>div{
        &:first-child {
            border-right:solid thin #ccc;
            background-color:#444a42;
            
        }
        &:last-child {
            text-align:left
            padding-left:5px;  
            
        }
        flex:1;        
    }

    &>.time{
        color:#ffffff;
        font-size:26px;
        font-weight:500;
        padding:10px;

        @media only screen and (max-width: 600px){
            color:#ffffff;
            font-size:30px;
            font-weight:500;
            padding:30px 10px;
        }

    }

    

    .dateDay{
        font-size:16px;
        display:block;
        padding:3px;
        color:#444a42;
        font-weight:600;

        @media only screen and (max-width: 600px){
            display:inline;
            text-align:center;
            font-size:18px;
        }
    }

   
`

export const RStyled = styled.div`
    flex:1;

    @media only screen and (max-width: 600px){
        display:none;
        visibility:hidden;

    }

    &>div{
        flex:1;
        text-align:right;

        &:last-child {
            padding-left:5px;  
        }

        &>select{
            width:200px;
        }

        &>ul{
            list-style-type:none;
            margin:0;
            padding:3px;

            
            &>li{
                display:inline-block;
                margin-right:10px;
                cursor:pointer;
                color:#444a42;
                font-weight:600;
                padding:3px;
                border-radius:5px;

                background-color:${props=>{return(props.type === 'List' ? "#F7B733":"white")}};

                :hover{
                    background-color:#F7B733;
                    color:white;
                }


               
            }
        }
    }

    .lis{
        color:#332b2b99;
        font-size:16px;
        display:block;
        font-weight:500;
        padding:3px;
    }


`

export const Vids = styled.ul`
list-style-type:none;
padding:5px;
flex-direction:row;
display:flex;

align-items:center;
flex-wrap:wrap;
justify-content:space-around;
font-size:12px;

.Desc{
    padding:10px;
    line-height:1.2;
    color: #988d8a;
}


.textTime{
    color: #a9a9a9;
    font-size: 14px;
    font-weight: 400;
    padding:20px 0px;
    margin-bottom:10px;
}

.vidTitle{
    color: #6c7770;
    font-size: 16px;
    font-weight:bold;
    text-align:center;
    margin:10px;    
}

.licenseBtn{
    background:#f04f38;
    border-radius:10px;
    padding:10px;
    text-align:center;
    width:200px;
    color:#ffffff;
    font-size:12px;
    font-weight:800;
    margin:10px auto;
    cursor:pointer;

    :hover{
        background:#a74537;

    }
}

&>li{
    margin:5px auto;
    padding:10px;
    max-width:${props=>{return(props.type === "List" ? "800px":"300px")}};
    min-width:${props=>{return(props.type === "List" ? "800px":"300px")}};
    border-radius 20px 0 20px 0;
    background:#f7efed61;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 2px 0 rgba(0, 0, 0, 0.19);
    
    @media only screen and (max-width: 900px){
        margin:5px auto;
        padding:10px;
        max-width:${props=>{return(props.type === "List" ? "450px":"300px")}};
        min-width:${props=>{return(props.type === "List" ? "450px":"300px")}};
        border-radius 20px 0 20px 0;
        background:#f7efed61;
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 2px 0 rgba(0, 0, 0, 0.19);
    }

    .left{
        border:solid thin #ccc;
        padding:10px;
        font-size:20px;
        flex:1;
        display:flex;
        max-width:30px;
    }
    .right{
        border:solid thin #ccc;
        padding:10px;
        font-size:20px;
        flex:1;
        display:flex;
        max-width:230px;
    }
}

`

export const TwoColumn = styled.ul`
margin:0px;
padding:0;
list-style-type:none;
display:flex;
border-bottom:solid thin #928e8e2e;

&>li{
    padding:10px 0 2px 0;
    font-size:14px;
    font-weight:800;
    border-bottom;
    color:#6c7770;

    :first-child{
        display:flex;
        flex:1;
    }
}
`

export const Card = styled.div`
display:flex;
flex:1;
border:solid thin #ccc;
`

export const Content= styled.div`

max-width:1100px;
align-items:center;
margin:0 auto;
`
export const AppStyled = styled.div`
align-item:center;
justify-content:center;
`

export const VideoCont = styled.div`
display:flex;

.frame{
    border:solid thin #ccc;
    flex:1;
    height:30vh;
    
}

`