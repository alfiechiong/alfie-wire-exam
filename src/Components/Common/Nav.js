import React from 'react';
import {NavStyled,LStyled,RStyled} from './Styles'
import Moment from 'react-moment';
import Clock from 'react-live-clock';
import { FaThList,FaTh } from 'react-icons/fa'
import {Consumer} from './AppProvider';
import {withContext} from "./withContext"



 

export default withContext((props)=>{

    const handleChange = (e)=>{
        console.log(e.target.value)
        props.state.setFilter(e.target.value)
    }

    return(
    <NavStyled>
        <LStyled>
            <div className='time'>
                    <Clock format={'hh:mm:ss A'} ticking={true}  /> 
            </div>
            <div>
                    <Moment className="dateDay" local format="dddd">
                        {new Date()}
                    </Moment>
                    <Moment className="dateDay" local format="MMMM DD ,YYYY">
                        {new Date()}
                    </Moment>
            </div>
        </LStyled>
        <Consumer>
            {(context)=>(
            <RStyled>
                <div>
                    <ul>
                        <li type={context.type} onClick={()=>{context.setType("List")}}>List View <FaThList /></li>
                        <li type={context.type} onClick={()=>{context.setType("Grid")}}>Grid View <FaTh /></li>
                    </ul>
                </div>
                <div>
                    <select onChange={handleChange}>
                        <option value="all">All</option>
                        <option value="day">Last 24 hours</option>
                        <option value="week">Last 7 days</option>
                    </select>
                </div>
            </RStyled>
            )}
        </Consumer>
    </NavStyled>
    )
})
