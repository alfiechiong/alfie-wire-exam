import React from 'react';
import './App.css';
import Main from './Components/Main'

import Header from './Components/Common/Header'
import Nav from './Components/Common/Nav'
import {Content,AppStyled} from './Components/Common/Styles'
import AppProvider from './Components/Common/AppProvider'

function App() {
  return (
	<AppProvider>
    <AppStyled>
            <Header />
           	<Nav />
            <Content>
            	<Main />
            </Content>
    </AppStyled>
	</AppProvider>
  );
}

export default App;
