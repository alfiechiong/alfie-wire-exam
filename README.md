React APPLICATION

DEPENDENCIES

1."react-moment":
    - Automatically translates dates and transform date ranges 

2."react-icons":
    - List of icons to be used comming from FA and other sources

3. "react-infinite-scroll-component":
    - Will Automatically load data on scroll

4."react-live-clock":
    - Automatically create a ticking clock has dependency on momentjs

5."styled-components":
    - TO easily interpolate data of style thought props this will help also to make components and global based styles also easy to implement theming styles

RUN on local:

yarn 

yarn start